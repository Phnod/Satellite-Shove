﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleGravity : MonoBehaviour 
{
	public ParticleSystem _ParticleSystem = null;
	public static float _GravityAmount = 0.01f;
	public float _ParticleMass = 0.1f;
	
    private ParticleSystem.Particle[] _ParticlesArray = null;
	private int _NumActiveParticles = 0;



	void Awake () 
	{
		Setup();
	}

	void Setup()
	{
		if(_ParticleSystem == null && GetComponent<ParticleSystem>() != null)
		{
			_ParticleSystem = GetComponent<ParticleSystem>();
		}
		
        if (_ParticleSystem != null)
        {
            _ParticlesArray = new ParticleSystem.Particle[_ParticleSystem.main.maxParticles];
        }
	}
	
	void FixedUpdate () 
	{
        if(_ParticleSystem != null)
        {
            _NumActiveParticles = _ParticleSystem.GetParticles(_ParticlesArray);

            for(int iParticle = 0; iParticle < _NumActiveParticles; iParticle++) 
			{
				// Fix this when I get back
				Vector3 forceSum = Vector3.zero;
				for(int i = 0; i < Gravity.gravityList.Count; ++i)
				{
					if(Gravity.gravityList[i].gameObject.activeInHierarchy)
					{
						float distance = Vector3.Distance(Gravity.gravityList[i].transform.position, _ParticlesArray[iParticle].position);
						distance = distance * distance;
				
						float force = (_GravityAmount * Gravity.gravityConst / Time.fixedDeltaTime) * (_ParticleMass * Gravity.gravityList[i].getMass()) / distance;
						forceSum += (Gravity.gravityList[i].transform.position - _ParticlesArray[iParticle].position).normalized * force;
					}
				}
				_ParticlesArray[iParticle].velocity += (forceSum / _ParticleMass) * Time.fixedDeltaTime;

                //_ParticlesArray[iParticle].velocity = Vector3.zero;	
				//float distance = Vector3.Distance(_ParticlesArray[iParticle].position, _ParticlesTarget);
                //_ParticlesArray[iParticle].position = Vector3.Lerp(_ParticlesArray[iParticle].position, _ParticlesTarget, _SuccAmount / Mathf.Pow(distance, _DistancePow));
            }

            _ParticleSystem.SetParticles(_ParticlesArray, _NumActiveParticles);
        }
	}
}
