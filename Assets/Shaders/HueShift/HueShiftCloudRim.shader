﻿Shader "Stephen/HueShiftCloudRim"
{
	Properties
	{
		_CloudTex ("Cloud", 2D) = "grey" {}
		_CloudParams("Cloud Speed & Scale", Vector) = (1,1,1,1)
		_HueShiftIntensity("Hue Shift Intensity", Float) = 1
		_HueShiftSpeed("Hue Shift Speed", Float) = 0
		_HueShiftOffset("Hue Shift Offset", Float) = 0
		_RimPow("Rim Power", Float) = 1
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "../Util/SatMath.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				half3 normal : TEXCOORD1;
				half3 worldPos : TEXCOORD2;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _CloudTex;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _CloudParams;
			float4 _Color;
			float _HueShiftIntensity;
			float _HueShiftOffset;
			float _HueShiftSpeed;
			float _RimPow;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos.xyz = mul(unity_ObjectToWorld, v.vertex);
				o.normal = v.normal;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 viewDir = float3(0, 0, -1);
				if(UNITY_MATRIX_P[3][3] == 0.0)
					viewDir = normalize(_WorldSpaceCameraPos - i.worldPos.xyz);

				fixed4 col = tex2D(_MainTex, i.uv) * _Color;

				float2 timeOffset = _Time.r * _CloudParams.xy;
				float cloud = 	tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv - timeOffset * float2(1, 0.9) + float2(0.5, 0.1)));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(-1.5, 1.5) + float2(0.75, 0.2)));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(1.5, -1.5) + float2(0.25, 0.3)));
				cloud = (cloud * 0.5 - 1) * _HueShiftIntensity;
								
				float3 hsv = RGBtoHSV(col.rgb);
				hsv.x += cloud + _Time.r * _HueShiftSpeed + _HueShiftOffset;
				col.rgb = HSVtoRGB(hsv);

				half rim = (dot (viewDir, i.normal));
				col *= 1 - saturate(pow(rim, _RimPow));
				//col.rgb = rim;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
