﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    public GameObject _Projectile;
    [SerializeField]
    GameObject _CrosshairPrefab;
    
    [SerializeField]
    GameObject _ChargeUp;
    [SerializeField]
    Transform _ShootOrigin;
    Material _ChargeUpMaterial;
    Camera _Camera;
    GameObject _Crosshair;
    GameObject _Satellite;
    GameObject _Target;
    int _Score;
    int _HighestScore;
    Text _ScoreField;
    Text _HighestScoreField;
    bool fired = false;
    float _aimTimer = 0.0f;
    float _shootTimer = 0.0f;

    enum LauncherState { Aiming, Power, Shot };
    LauncherState state = LauncherState.Aiming;

    void Start()
    {
        _Camera = Camera.main;
        _ChargeUpMaterial = _ChargeUp.GetComponent<MeshRenderer>().material;
        _Crosshair = Instantiate(_CrosshairPrefab);
        _Target = GameObject.FindGameObjectWithTag("Target");
        _ScoreField = GameObject.FindGameObjectWithTag("ScoreNum").GetComponent<Text>();
        _HighestScoreField = GameObject.FindGameObjectWithTag("ScoreHighestNum").GetComponent<Text>();
    }
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            
            SceneManager.LoadScene(currentScene.buildIndex);
        }
        
        int buildOffset = -1;
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(1 + buildOffset);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            SceneManager.LoadScene(2 + buildOffset);
        }
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            SceneManager.LoadScene(3 + buildOffset);
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        Vector3 crosshairPosition = _Camera.ScreenToWorldPoint(Input.mousePosition);
        crosshairPosition.z = this.transform.position.z;

        switch(state)
        {
            case LauncherState.Aiming:
            _Crosshair.transform.position = crosshairPosition;
            this.transform.rotation = Quaternion.LookRotation(crosshairPosition - this.transform.position, Vector3.forward);
            if(Input.GetMouseButtonDown(0))
            {
                state = LauncherState.Power;
                _aimTimer = 0.0f;
            }

            break;
            case LauncherState.Power:

            float power = 1.0f - Mathf.Abs(Mathf.Sin(_aimTimer * 2.0f + Mathf.PI * 0.5f));
            SetThreshold(power);
            _aimTimer += Time.deltaTime;

            if(Input.GetMouseButtonDown(1))
            {
                state = LauncherState.Aiming;
                _shootTimer = 0.0f;
                SetThreshold(0.0f);
            }
            
            if(Input.GetMouseButtonDown(0))
            {
                state = LauncherState.Shot;
                SetThreshold(0.0f);
                _Satellite = Instantiate(_Projectile, _ShootOrigin.position, _ShootOrigin.rotation);
                _Satellite.GetComponent<Rigidbody>().AddForce((crosshairPosition - this.transform.position).normalized * power * 30000.0f);
            }

            break;
            case LauncherState.Shot:

            _shootTimer += Time.deltaTime;
            
            if(_Satellite != null)
            {
                float distance = Vector3.Distance(_Satellite.transform.position, _Target.transform.position);
                distance = Mathf.InverseLerp(35.0f, 0.0f, Mathf.Pow(distance, 1.15f));
                _Score = (int)(10000.0f * Mathf.Pow(distance, 10.1f) / Mathf.Pow(Mathf.Max(_shootTimer * 0.25f, 1.0f), 1.05f));
                //_Score = (int)(10000.0f / Mathf.Pow(Mathf.Max(_shootTimer * 0.25f, 1.0f), 1.05f));
                //Debug.Log(_shootTimer);
                //_Score = (int)(10000.0f / Mathf.Max(Mathf.Pow(distance, 1.15f) - 10.0f, 1.0f) / Mathf.Pow(Mathf.Max(_shootTimer, 3.0f), 1.5f));
                if(_Score > _HighestScore)
                {
                    _HighestScore = _Score;
                    _HighestScoreField.text = _HighestScore.ToString();
                }
                _ScoreField.text = _Score.ToString();
            }

            break;
            default:

            break;
        }
    }

    void SetThreshold(float amount)
    {        
        _ChargeUpMaterial.SetFloat("_Threshold", amount);
    }
    void Fire()
    {
        fired = true;
    }
}
