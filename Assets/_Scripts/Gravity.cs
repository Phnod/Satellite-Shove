﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Gravity : MonoBehaviour 
{
	public static List<Gravity> gravityList = new List<Gravity>();
	public static float gravityConst = 0.01f;

	// Public Variables
	public Vector3 initForce;
	public bool _attractor = true;
	public bool _attracted = true;
	public bool _destructable = false;
	public GameObject _destroyPrefab;

	// Private Variables
	bool init = false;
	Rigidbody rb;
	TrailRenderer tr;
    GameObject lastCollision;

	public float getMass()
	{
		return rb.mass;
	}

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody>();
		if(!init && _attractor)
		{
			gravityList.Add(this);
		}

		tr = GetComponent<TrailRenderer>();
		if(tr != null)
		{
			tr.widthMultiplier = Mathf.Min(transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}

		rb.AddForce(initForce * rb.mass);
	}
	
	void ApplyForce()
	{
		Vector3 forceSum = Vector3.zero;
		for(int i = 0; i < gravityList.Count; ++i)
		{
			// I don't think a check needs to be done if it's doing gravity against itself 
			// Since it should be zero anyways, but what the heck
			if(gravityList[i] != this && gravityList[i].gameObject.activeInHierarchy)
			{
				float distance = Vector3.Distance(gravityList[i].transform.position, this.transform.position);
				distance = distance * distance;
				
				float force = (gravityConst / Time.fixedDeltaTime) * (this.rb.mass * gravityList[i].rb.mass) / distance;
				forceSum += (gravityList[i].transform.position - this.transform.position).normalized * force;
			}
		}
		rb.AddForce(forceSum * Time.fixedDeltaTime);
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		if(_attracted)
		{
			ApplyForce();
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if(_destructable)
        {
            if (_destroyPrefab != null)
            {
                //Instantiate(_destroyPrefab, this.transform.position, Quaternion.LookRotation(-(collision.transform.position - this.transform.position).normalized, Vector3.back));
				Instantiate(_destroyPrefab, this.transform.position, Quaternion.identity);
			}
            Destroy(this.gameObject);
		}
	}

	void OnDestroy()
	{
        gravityList.Remove(this);
	}
}
