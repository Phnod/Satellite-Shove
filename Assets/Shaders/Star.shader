﻿Shader "Stephen/Star"
{
	Properties
	{
		_CloudTex("Cloud", 2D) = "white" {}
		_CloudParams("Cloud Speed & Scale", Vector) = (1,1,1,1)
		_CloudIntensity("Cloud Intensity", Float) = 1
		_CloudThreshold("Cloud Threshold", Float) = 0
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] 
		_Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _CloudTex;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _CloudParams;
			float _CloudIntensity;
			float _CloudThreshold;
			float4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;

				float2 timeOffset = _Time.r * _CloudParams.xy;
				float cloud = tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset));
				cloud += tex2D(_CloudTex, _CloudParams.zw * (i.uv - timeOffset * float2(1, 0.9) + float2(0.5, 0.1)));
				cloud += tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(-1.5, 1.5) + float2(0.75, 0.2)));
				cloud += tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(1.5, -1.5) + float2(0.25, 0.3)));
				cloud = (cloud * 0.5 - 1 + _CloudThreshold) * _CloudIntensity;

				col *= cloud;
				//col *= cloud + (1 - _CloudIntensity);
					
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
