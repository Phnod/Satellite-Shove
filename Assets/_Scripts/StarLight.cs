﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarLight : MonoBehaviour 
{
	public Color _color = Color.white;
	public bool _randomColor = false;
	Color originalColor;
	Material mat;
	Light _light;

	// Use this for initialization
	void Start () 
	{
		mat = GetComponent<MeshRenderer>().material;
		if(_randomColor)
		{
			_color = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 1.0f, 1.0f);
		}

		Vector4 cloudParams = mat.GetVector("_CloudParams");
		cloudParams.x *= Random.Range(1.0f, 1.5f);
		cloudParams.y *= Random.Range(1.0f, 1.5f);
		transform.Rotate(new Vector3(0.0f, Random.Range(0.0f, 360.0f), 0.0f), Space.Self);
		mat.SetVector("_CloudParams", cloudParams);
		originalColor = mat.GetColor("_Color");
		mat.SetColor("_Color", _color * originalColor) ;

		_light = GetComponent<Light>();
		_light.color = SatMath.AdjustHSV(_color, 0.0f, 0.15f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
}
