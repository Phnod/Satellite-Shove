﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialScroller : MonoBehaviour
{
    private Material _Mat;
    private Vector2 _Tile_Offset;
    public float _Tile_Offset_Speed_X = 0.0f;
    public float _Tile_Offset_Speed_Y = 0.0f;

    private void Start()
    {
        _Mat = gameObject.GetComponent<MeshRenderer>().material;

        _Tile_Offset_Speed_X *= 0.01f;
        _Tile_Offset_Speed_Y *= 0.01f;
    }

    // Update is called once per frame
    void Update ()
    {
        if(_Mat)
        {
            _Tile_Offset.x += _Tile_Offset_Speed_X * (Time.deltaTime * 60.0f);
            _Tile_Offset.y += _Tile_Offset_Speed_Y * (Time.deltaTime * 60.0f);

            _Mat.SetTextureOffset("_MainTex", _Tile_Offset);
        }

    }
}
