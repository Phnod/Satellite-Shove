﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLoopBack : MonoBehaviour 
{
	public ParticleSystem _ParticleSystem = null;


	public Vector3 _ParticlesTarget = Vector3.zero;
	private bool _WorldPosition = false;
    private ParticleSystem.Particle[] _ParticlesArray = null;
	Vector3[] _ParticleDirection;
	
	// Particle Params
	private float _ParticleLifeTime;

	void Awake () 
	{
		Setup();
	}
	
	int _NumActiveParticles = 0;
	float _TimeProgress = 0.0f;
	float _TimeSpeed;
	public Vector3 _Scale = Vector3.one;

	public bool autoTangent = true;
	public AnimationCurve HorizontalMovement = new AnimationCurve(
		new Keyframe(0.0f, 0.0f, 2.5f, 2.5f), new Keyframe(0.4f, 1.0f, 0.0f, 0.0f), new Keyframe(0.8f, 0.1f, -1.375f, -1.375f), new Keyframe(1.0f, 0.0f, 0.0f, 0.0f));
	public AnimationCurve VerticalMovement = new AnimationCurve(
		new Keyframe(0.0f, 0.0f, 0.0f, 0.0f), new Keyframe(0.2f, 0.1f, 1.375f, 1.375f), new Keyframe(0.6f, 1.0f, 0.0f, 0.0f), new Keyframe(1.0f, 0.0f, -2.5f, -2.5f));
	private float HorizontalMovementResult;
	private float VerticalMovementResult;


	bool _DebugParticle = true;

	void OnValidate()
	{
		//if(autoTangent) 
		//{
		//	for(int i = 0; i < HorizontalMovement.keys.Length; ++i)
		//	{
 		//		//HorizontalMovement.SmoothTangents(i, 0); //zero weight means average
		//		//print();
		//		print("Tangent #" + i + "\t" + HorizontalMovement.keys[i].inTangent + " " + HorizontalMovement.keys[i].outTangent);
		//	}	
		//	for(int i = 0; i < VerticalMovement.keys.Length; ++i)
		//	{
 		//		//VerticalMovement.SmoothTangents(i, 0); //zero weight means average
		//		print("Tangent #" + i + "\t" + VerticalMovement.keys[i].inTangent + " " + VerticalMovement.keys[i].outTangent);
		//	}	
		//}
	}

	void Update () 
	{
		if(_ParticleSystem != null)
        {
			if(_DebugParticle)
			{
			}

			_TimeProgress += _TimeSpeed * Time.deltaTime;
			if(_TimeProgress > 1.0f)
				_TimeProgress = 0.0f;
			

			_NumActiveParticles = _ParticleSystem.GetParticles(_ParticlesArray);
            _ParticlesTarget = transform.position;
            if (!_WorldPosition)
			{
				_ParticlesTarget -= _ParticleSystem.transform.position;
			}

			HorizontalMovementResult = HorizontalMovement.Evaluate(_TimeProgress);
			VerticalMovementResult = VerticalMovement.Evaluate(_TimeProgress);

            for(int iParticle = 0; iParticle < _NumActiveParticles; iParticle++) 
			{
				{
					Vector3 newPosition = new Vector3();
					newPosition = _ParticlesArray[iParticle].velocity * HorizontalMovementResult;
					newPosition.y = VerticalMovementResult;
                    _ParticlesArray[iParticle].position = _ParticlesTarget + Vector3.Scale(newPosition,_Scale);
					
					//_ParticlesArray[iParticle].position =  Quaternion.AngleAxis(_RotateAmount * Time.deltaTime / Mathf.Max(Mathf.Pow(distance, _DistancePow), 0.01f), Vector3.forward) * _ParticlesArray[iParticle].position;
					
                }
            }
            _ParticleSystem.SetParticles(_ParticlesArray, _NumActiveParticles);
		}
	}

	void Setup()
	{
        if (_ParticleSystem != null)
        {
            _ParticlesArray = new ParticleSystem.Particle[_ParticleSystem.main.maxParticles];
			_ParticleDirection = new Vector3[_ParticleSystem.main.maxParticles];

			_NumActiveParticles = _ParticleSystem.GetParticles(_ParticlesArray);
			for(int iParticle = 0; iParticle < _NumActiveParticles; iParticle++) 
			{
				_ParticleDirection[iParticle] = _ParticlesArray[iParticle].velocity;
				_ParticleDirection[iParticle].y = 0.0f;
				_ParticleDirection[iParticle].Normalize();
			}

            _WorldPosition = _ParticleSystem.main.simulationSpace == ParticleSystemSimulationSpace.World;

			var particleMain = _ParticleSystem.main;
			_ParticleLifeTime = particleMain.startLifetime.constant;
			_TimeSpeed = 1.0f / _ParticleLifeTime;
        }
	}
}
