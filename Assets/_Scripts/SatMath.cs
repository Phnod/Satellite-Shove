﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatMath
{
	static public Color AdjustHSV(Color color, float H, float S, float V)
	{
		float _H;
		float _S;
		float _V;
		Color.RGBToHSV(color, out _H, out _S, out _V);
		return Color.HSVToRGB(_H + H, _S * S, _V * V);
	}
}
