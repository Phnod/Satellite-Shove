﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSucker : MonoBehaviour 
{
    public ParticleSystem _ParticleSystem = null;
	[Header("Succ?")]
	public float _SuccAmount = 0.01f;
	public float _DistancePow = 1.0f;
	[Header("Rotate?")]
	[Tooltip("Should the particles spin around as they get closer")]
	public bool _RotateSucc = false;
	public float _RotateAmount = 0.0f;
    public Vector3 _RotateAround = Vector3.forward;
	private bool _WorldPosition = false;
    private ParticleSystem.Particle[] _ParticlesArray = null;

	void Awake () 
	{
		Setup();
	}
	
	int _NumActiveParticles = 0;
	
    private Vector3 _ParticlesTarget = Vector3.zero;
	void Update () 
	{
        if(_ParticleSystem != null)
        {
            _NumActiveParticles = _ParticleSystem.GetParticles(_ParticlesArray);
            _ParticlesTarget = transform.position;
            if (!_WorldPosition)
                _ParticlesTarget -= _ParticleSystem.transform.position;

            for(int iParticle = 0; iParticle < _NumActiveParticles; iParticle++) 
			{ 
				{
                    _ParticlesArray[iParticle].velocity = Vector3.zero;	
					float distance = Vector3.Distance(_ParticlesArray[iParticle].position, _ParticlesTarget);
                    _ParticlesArray[iParticle].position = Vector3.Lerp(_ParticlesArray[iParticle].position, _ParticlesTarget, _SuccAmount / Mathf.Pow(distance, _DistancePow));
					

					if(_RotateSucc)
					{
						_ParticlesArray[iParticle].position =  Quaternion.AngleAxis(_RotateAmount * Time.deltaTime / Mathf.Max(Mathf.Pow(distance, _DistancePow), 0.01f), _RotateAround) * _ParticlesArray[iParticle].position;
					}
                }
            }
            _ParticleSystem.SetParticles(_ParticlesArray, _NumActiveParticles);
        }
	}

	void Setup()
	{
        if (_ParticleSystem != null)
        {
            _ParticlesArray = new ParticleSystem.Particle[_ParticleSystem.main.maxParticles];
            _WorldPosition = _ParticleSystem.main.simulationSpace == ParticleSystemSimulationSpace.World;
            if(_ParticleSystem.main.prewarm)
            {
                for(int i = 0; i < 400; i++)
                {
                    _ParticleSystem.Simulate(Time.deltaTime, true, false);
                    Update();
                }
                _ParticleSystem.Play();
            }
        }
	}
}
